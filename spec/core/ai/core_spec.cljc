(ns core.ai.core-spec
  (:require [core.ai.core :refer :all]
            [speclj.core :refer :all]
            [core.constants :refer [field-width field-height pieces]]))
